from controller import Robot
class MyRobotController (Robot):
    # Variables
    TIME_STEP = 12
    maxSpeed = 5
    state= 0
    turnState = 0
    duration = 0
    ps = []
    lightgoal = False
    sensors = [0,0,0,0,0,0,0,0,0,0,0,0]
    status = True
    psValues = []
    psNames = ["prox.horizontal.0", "prox.horizontal.1", "prox.horizontal.2", "prox.horizontal.3", "prox.horizontal.4", "prox.horizontal.5", "prox.horizontal.6", "prox.ground.0", "prox.ground.1"]
    robotInfo = [0, 0, 0, 0, 0,0,0]
    wheelStep = 0.0
    temp = 0.0
    counter = 0
    pushing = False
    priviousStep = 0
    nextStep = ""
    distance = 0.0
    goOut = True
    avoidDone = False
    currentState = ""
    target = {
        "distance":0,
        "time":0
    }
    turnAngel = 0
    turnInfo = {
        "direction":"L",
        "duration": 0.0,
        "self.L_MotorSpeed": 0,
        "self.R_MotorSpeed":0,
        "turnCount": 0
    }
    
    def sensorsRead(self):
        for i in range(len(self.psNames)):
           self.sensors[i] = self.ps[i].getValue()
        self.sensors[9] = self.getLightSensor("light0").getValue()
        self.sensors[10] = self.getLightSensor("light1").getValue()
        self.sensors[11] = self.getTouchSensor("force").getValue()
        if self.counter == 1:
            self.temp = self.L_Motor.getPositionSensor().getValue()
        self.counter +=1
        self.wheelStep = self.L_Motor.getPositionSensor().getValue() - self.temp
        self.priviousStep = self.wheelStep
        
    
    # Methods
    def init(self):
        self.state = "findTarget"
        self.L_Motor = self.getMotor('motor.left')
        self.R_Motor = self.getMotor('motor.right')
        self.R_Motor.setPosition(float('inf'))
        self.L_Motor.setPosition(float('inf'))

        # Enable the Sensors
        for i in range(len(self.psNames)):
            self.ps.append(self.getDistanceSensor(self.psNames[i]))
            self.ps[i].enable(self.TIME_STEP)
        self.getLightSensor("light0").enable(self.TIME_STEP)
        self.getLightSensor("light1").enable(self.TIME_STEP)
        self.getTouchSensor("force").enable(self.TIME_STEP)
        self.L_Motor.getPositionSensor().enable(self.TIME_STEP)
        self.R_Motor.getPositionSensor().enable(self.TIME_STEP)
    #
    # 
    # Check Border Function    
    def checkLight(self):
        if self.sensors[9]> 750 and self.sensors[10] > 750:
            self.lightgoal == True
            self.robotInfo[6] = 1
            return True
        else:
            self.robotInfo[6] = 0
            return False
    #############################################################
    def checkBorder(self):
        if self.sensors[7] > 765 or self.sensors[8] > 765:
            self.robotInfo[4] = 1
            return True
        else:
            self.robotInfo[4] = 0    
            return False
    ############################################################
    def foundObject(self):
        if self.sensors[11]> 0.2:
            return True
        return False
    ############################################################
   
    ######################### Find Target Function ####################################
    def findTarget(self):
        self.L_Motor.setVelocity(self.maxSpeed)
        self.R_Motor.setVelocity(self.maxSpeed)
            
            #### TODOO Turn Function
    def turn(self):
        if self.getTime() - self.duration > self.turnInfo["duration"]:
            return True
        self.L_Motor.setVelocity(-self.maxSpeed)
        self.R_Motor.setVelocity(self.maxSpeed)
        return False
        
    def stop(self):
        self.L_Motor.setVelocity(0)
        self.R_Motor.setVelocity(0)
        self.state ="asd"
def start(self):
        self.init()
        while self.status: 
            self.robotInfo[0] = self.L_Motor.getVelocity()
            self.robotInfo[1] = self.R_Motor.getVelocity()
            self.sensorsRead() 
            ########################### find Target ##############################
            if self.state == "findTarget":
                self.currentState = "findTarget"
                # Light Was Found
                if self.checkLight():
                    self.lightgoal = True
                    self.turnInfo["duration"] = 1
                    self.turnInfo["direction"] = "L"
                    self.target["distance"] = 0
                    self.duration = self.getTime()
                    self.state = "Turn"
                    self.turnInfo["duration"] = 1
                    self.nextStep = "Search"
                    self.goOut = True
                # if  not targetEreaFound:
                else:
                    self.L_Motor.setVelocity(self.maxSpeed)
                    self.R_Motor.setVelocity(self.maxSpeed)
                    if self.sensors[11]> 0.2:
                        self.duration = self.getTime()
                        self.state = "Back"
                        self.nextStep = "Turn"
                        self.turnInfo["turnCount"] +=1
                        if self.turnInfo["turnCount"] %3 ==0:
                            self.turnInfo["direction"] = "L"
                        else:
                            self.turnInfo["direction"] = "R"
                        self.turnInfo["duration"] = 6.4/self.maxSpeed
                    if self.checkBorder():
                        if self.sensors[7] > self.sensors[8]:
                            self.turnInfo["direction"] = "R"
                        else:
                            self.turnInfo["direction"] = "L"
                        self.state = "Back"
                        self.nextStep = "Turn"
                        self.turnInfo["duration"] = 6.4/self.maxSpeed
            ####################################### Search State #############################
            if self.state == "Search":
                self.currentState = "findTarget"
                 
                # Thymio here after turning & the light is on
                # Bring the boxes Algorithm
                self.L_Motor.setVelocity(self.maxSpeed)
                self.R_Motor.setVelocity(self.maxSpeed)
                if self.goOut:
                    self.distance += self.getTime() - self.duration 
                    if self.checkBorder():
                        print("############################################")
                        self.state = "Turn"
                        self.duration = self.getTime()
                        self.turnInfo["duration"] = 2.495 # 180
                        self.nextStep = "goToLight"
                        self.goOut = False
                    if self.foundObject():

                        self.state= "Avoid"
                        self.nextStep = "goToLight"
                        self.duration = self.getTime()
                        self.turnInfo["direction"] = "R"
                        self.turnInfo["duration"] = 2.495 # 180 
                        self.goOut = False
                    if self.checkLight() and self.foundObject():
                        self.state = "Turn"
                        self.distance = 0
                        self.duration = self.getTime()
                        self.turnInfo["duration"] = self.getTime()%3+1 # 180
                        self.nextStep = "goToLight"
                                          
                else:
                    self.distance -= self.getTime() - self.duration + 1.5
                    if self.distance < 0:
                        self.state = "Turn"
                        self.duration = self.getTime()
                        self.turnInfo["duration"] = 1 # 180 
                        self.goOut = True
                    if self.checkBorder():# miss the light
                        self.state = "Turn"
                        self.nextStep = "goToLight"
                        self.lightgoal = False
                        self.duration = self.getTime()
                        self.turnInfo["duration"] = 2.495 # 180 
                    if self.checkLight():
                        self.distance= 0
                        self.state = "Turn"
                        self.duration = self.getTime()
                        self.turnInfo["duration"] = 1 # 180 
                        self.nextStep = "Search"
                        self.goOut = True
                        
                        
                    
                 
                
            ######################################  Back State ###############################
            if self.state == "Back":
                self.currentState = "findTarget"
                self.L_Motor.setVelocity(-2)
                self.R_Motor.setVelocity(-2)
                if self.getTime() - self.duration > 1:
                    self.duration = self.getTime()
                    self.state = self.nextStep
            ###################################### Back End ##################################
            ###################################### Turn State ################################
            if self.state == "Turn":
                self.robotInfo[5] = 0
                self.currentState = "findTarget"
                if self.turnInfo["direction"] == "L":
                    self.L_Motor.setVelocity(-5)
                    self.R_Motor.setVelocity(5)
                else:
                    self.L_Motor.setVelocity(5)
                    self.R_Motor.setVelocity(-5)
                if self.getTime() - self.duration > self.turnInfo["duration"]:
                    self.duration = self.getTime()
                    # self.turnInfo["duration"] = 0
                    if self.lightgoal:
                        self.state = "Search"
                    else:
                        self.state = "findTarget"
                # self.goOut = not self.goOut
            ######################################## End Turn ################################
            ######################################## GoToLight ###############################
            if self.state == "goToLight":
                self.distance -= self.getTime() - self.duration - 1.5
                self.currentState = "findTarget"
                self.L_Motor.setVelocity(self.maxSpeed)
                self.R_Motor.setVelocity(self.maxSpeed)
                if self.distance <0:
                    self.state = "Turn"
                    self.duration = self.getTime()
                    self.turnInfo["duration"] = 2.495 # 180
                    self.nextStep = "Search"
                    self.distance = 0.0
                    self.goOut = True
                if self.foundObject():
                    self.robotInfo[5] = 1
                else:
                    self.robotInfo[5] = 0
                # if self.checkBorder():# miss the light
                #     self.state = "Turn"
                #     self.nextStep = "findTarget"
                #     self.lightgoal = False
                #     self.duration = self.getTime()
                #     self.turnInfo["duration"] = 2.495 # 180 
                #     self.goOut = True
            ######################################## GoToLight End ###########################
            ######################################## Avoid state #############################
            if self.state == "Avoid":
                self.currentState = "findTarget"
                if robot.getTime() - self.duration < 0.5:
                    self.L_Motor.setVelocity(-5) #  VVVVVV
                    self.R_Motor.setVelocity(-5)
                elif robot.getTime() - self.duration > 0.5 and robot.getTime() - self.duration <1.75:
                    self.L_Motor.setVelocity(-5) # <=========
                    self.R_Motor.setVelocity(5)
                elif robot.getTime() - self.duration >1.75 and robot.getTime() - self.duration < 3:
                    self.L_Motor.setVelocity(5) # ^^^^^
                    self.R_Motor.setVelocity(5)
                elif robot.getTime() - self.duration > 3 and robot.getTime() - self.duration <4.25:
                    self.L_Motor.setVelocity(5) # =============>
                    self.R_Motor.setVelocity(-5)
                elif robot.getTime() - self.duration >4.25 and robot.getTime() - self.duration < 6.75:
                    self.L_Motor.setVelocity(5) # ^^^^^
                    self.R_Motor.setVelocity(5) 
                elif robot.getTime() - self.duration > 6.75 and robot.getTime() - self.duration < 8:
                    self.L_Motor.setVelocity(5) # ==========>
                    self.R_Motor.setVelocity(-5)
                elif robot.getTime() - self.duration > 8 and robot.getTime() - self.duration < 9.25:
                    self.L_Motor.setVelocity(5) # ^^^^
                    self.R_Motor.setVelocity(5)
                elif robot.getTime() - self.duration > 9.25 and robot.getTime() - self.duration < 10.5:
                    self.L_Motor.setVelocity(5)
                    self.R_Motor.setVelocity(-5)
                if robot.getTime() - self.duration > 10.5:
                    self.duration = robot.getTime()# reset the timer
                    self.state = self.nextStep
                    self.goOut = False
            if self.sensors[0] > 0 or self.sensors[1] > 0  or self.sensors[2] > 0 or self.sensors[3] > 0 or self.sensors[4] > 0:
                self.L_Motor.setVelocity(0) 
                self.R_Motor.setVelocity(0)
                self.robotInfo[3] = 1   
                if self.getTime() - self.duration > 2:
                    self.state = self.currentState
            else:
                self.robotInfo[3] = 0
            print(self.state)
            print(self.goOut)
            print(self.robotInfo)
            if self.step(self.TIME_STEP) == -1:  # quit the loop when Webots is about to quit
                break

robot = MyRobotController()
start(robot)