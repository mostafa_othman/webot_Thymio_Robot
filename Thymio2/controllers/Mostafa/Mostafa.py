"""Mostafa controller."""

from controller import Robot

robot = Robot()
TIME_STEP = 12
start = True
ps = []
psNames = ["prox.horizontal.0", "prox.horizontal.1", "prox.horizontal.2", "prox.horizontal.3", "prox.horizontal.4", "prox.horizontal.5", "prox.horizontal.6", "prox.ground.0", "prox.ground.1"]
leftMotor = robot.getMotor('motor.left')
rightMotor = robot.getMotor('motor.right')
MAX = ""
MIN = ""
timerPeriod = 0
distancePeriod = 0.0
STATE = "findBoard"
nextState = ""
spinInfo = {"direction":"left", "period":0, "spinMode":"", "angel":0.0, "nextState":""}
robotInfo = []
borderLimitFound = False
otherSideBorderFound = False
measuringBorderTime = 0.0
# Initialize Function
def initialize():
    leftMotor.setPosition(float("inf"))
    rightMotor.setPosition(float("inf"))
    # Enable Sensors
    for s in range(len(psNames)):
        ps.append(robot.getDistanceSensor(psNames[s]))
        ps[s].enable(TIME_STEP)
    robot.getLightSensor("light0").enable(TIME_STEP)
    robot.getLightSensor("light1").enable(TIME_STEP)
    robot.getTouchSensor("force").enable(TIME_STEP)
    leftMotor.getPositionSensor().enable(TIME_STEP)
    rightMotor.getPositionSensor().enable(TIME_STEP)

# Reading Sensors Values:
def readSensorsValues():
    global sensors 
    global sensorsInfo 
    global hs0
    global hs1
    global hs2
    global hs3
    global hs4
    global hs5
    global hs6
    global gs0
    global gs1
    global LS0
    global LS1
    global pre
    global STATE
    hs0 = ps[0].getValue()
    hs1 = ps[1].getValue()
    hs2 = ps[2].getValue()
    hs3 = ps[3].getValue()
    hs4 = ps[4].getValue()
    hs5 = ps[5].getValue()
    hs6 = ps[6].getValue()
    gs0 = ps[7].getValue()
    gs1 = ps[8].getValue()
    LS0 = robot.getLightSensor("light0").getValue()
    LS1 = robot.getLightSensor("light1").getValue()
    pre = robot.getTouchSensor("force").getValue()
    sensors = {"hs0":hs0, "hs1":hs1, "hs2":hs2, "hs3":hs3, "hs4":hs4, "hs5":hs5, "hs6":hs6}
    MAX = max(sensors, key=sensors.get)
    MIN = min(sensors, key=sensors.get)
    sensorsInfo = {"maxSensor": max(sensors, key=sensors.get), "maxValue":sensors.get(MAX), "minSensor":min(sensors, key=sensors.get), "minValue":sensors.get(MIN)  }
    robotInfo = [leftMotor.getVelocity(), leftMotor.getVelocity(), 0]
    

def run():
    initialize()
    STATE = "findBoard"
    measuringBorderTime = 0.0
    otherSideBorderFound = False
    borderLimitFound = False
    # Main loop:
    while start:
        readSensorsValues()
        if STATE == "findBoard":
            leftMotor.setVelocity(5)
            rightMotor.setVelocity(5)
            if gs0 > 760 or gs1 > 760:
                leftMotor.setVelocity(0)
                rightMotor.setVelocity(0)
                STATE = "recalibrating"
                timerPeriod = robot.getTime()

        if STATE == "recalibrating":
            if gs1 - gs0 > 5:
                STATE = "spin"
                spinInfo["direction"] = "right"
                spinInfo["spinMode"] = "recalibrating"

        if STATE == "spin":
            if spinInfo["spinMode"] == "recalibrating":
                if not borderLimitFound:
                    if spinInfo["direction"] == "right":
                        leftMotor.setVelocity(0.5)
                        rightMotor.setVelocity(-0.5)
                        if gs0 > 710:
                            timerPeriod = robot.getTime()
                            spinInfo["direction"] = "left"
                            borderLimitFound = True

                else:
                    if not otherSideBorderFound:
                        if spinInfo["direction"] == "left":
                            measuringBorderTime += robot.getTime() - timerPeriod
                            leftMotor.setVelocity(-0.5)
                            rightMotor.setVelocity(0.5)
                            if gs1 < 710:
                                otherSideBorderFound = True

                    else:
                        STATE = "stop"

        if STATE == "stop":
            leftMotor.setVelocity(0)
            rightMotor.setVelocity(0)
            

        print(STATE)
        print(spinInfo)
        print("gs0: " + str(gs0))
        print("gs1: " + str(gs1))
        if robot.step(TIME_STEP) == -1:  # quit the loop when Webots is about to quit
            break

run()