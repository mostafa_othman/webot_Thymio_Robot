import time
from controller import Robot
    
def run():
        robot = Robot()
        TIME_STEP = 12 
        motorSpeed = 4
        leftMotor = 0
        rightMotor = 0
        STATE = 0
        ps = []
        psNames = ["prox.horizontal.0", "prox.horizontal.1", "prox.horizontal.2", "prox.horizontal.3", "prox.horizontal.4", "prox.horizontal.5", "prox.horizontal.6", "prox.ground.0", "prox.ground.1"]
        hs0 = 0
        hs1 = 0
        hs2 = 0
        hs3 = 0
        hs4 = 0
        hs5 = 0
        hs6 = 0
        gs0 = 0
        gs1 = 0
        LS0 = 0
        LS1 = 0 # Back light Sensor
        pre = 0
        MAX = ""
        MIN = ""
        RUN = True
        count = 0
        step = 0
        cycle = 0.0
        currentTime = 0.0 
        movingOnY = True
        locationInfo = {"round": 0,"width":0.0, "hight":0.0}
        firstMotorStateInfo = {"leftMotor":0.0, "rightMotor":0.0}
        currentMotorInfo = {"leftMotor":0.0, "rightMotor":0.0}
        targetEreaFound = False
        targetPosition = {"x":0, "y":0,"angel":0} # Distance  
        Axse = ""
        turnOtherSide = 1
        movingInfo = {"direction":"forward","rightTurnCount":0,"leftTurnCount":0, "currentY":0.0, "currentX":0.0, "destanceY":0.0, "destanceX":0.0, "decreasment":0.2}
        sensors = {}
        sensorsInfo = {}
        timerPeriod = 0
        STATE = "Forward"
        robotInfo = [0,0,0,0,0,0,0]
        nextState = ""
        spinInfo = {"direction":"left", "duration":0.0, "spinMethod": "timeDuration", "pointPeriod": 0.0}
        leftMotor = robot.getMotor('motor.left')
        # leftMotor = robot.getMotor('motor.left')
        rightMotor = robot.getMotor('motor.right')
        leftMotor.setPosition(float("inf"))
        rightMotor.setPosition(float("inf"))
        time = 0
        goToTarget = False
        # Enable Motors Informations
        rightMotor.enableTorqueFeedback(TIME_STEP)# Motor Torque
        leftMotor.enableTorqueFeedback(TIME_STEP)
        # Enable all Sensors
        for s in range(len(psNames)):
            ps.append(robot.getDistanceSensor(psNames[s]))
            ps[s].enable(TIME_STEP)
        robot.getLightSensor("light0").enable(TIME_STEP)
        robot.getLightSensor("light1").enable(TIME_STEP)
        robot.getTouchSensor("force").enable(TIME_STEP)
        leftMotor.getPositionSensor().enable(TIME_STEP)
        rightMotor.getPositionSensor().enable(TIME_STEP)
        while RUN:
            if count == 1:# it should start after the next loop 
                firstMotorStateInfo["leftMotor"] = leftMotor.getPositionSensor().getValue()
                firstMotorStateInfo["rightMotor"] = rightMotor.getPositionSensor().getValue()
                cycle = robot.getTime() - currentTime
            currentTime = robot.getTime()
            step = currentMotorInfo["leftMotor"]
            # print(cycle)
            currentMotorInfo["leftMotor"] = leftMotor.getPositionSensor().getValue() - firstMotorStateInfo["leftMotor"]
            currentMotorInfo["rightMotor"] = rightMotor.getPositionSensor().getValue() - firstMotorStateInfo["rightMotor"]
            count+=1
            hs0 = ps[0].getValue()
            hs1 = ps[1].getValue()
            hs2 = ps[2].getValue()
            hs3 = ps[3].getValue()
            hs4 = ps[4].getValue()
            hs5 = ps[5].getValue()
            hs6 = ps[6].getValue()
            gs0 = ps[7].getValue()
            gs1 = ps[8].getValue()
            LS0 == robot.getLightSensor("light0").getValue()
            LS1 = robot.getLightSensor("light1").getValue()
            pre = robot.getTouchSensor("force").getValue()
            sensors = {"hs0":hs0, "hs1":hs1, "hs2":hs2, "hs3":hs3, "hs4":hs4, "hs5":hs5, "hs6":hs6}
            MAX = max(sensors, key=sensors.get)
            MIN = min(sensors, key=sensors.get)
            sensorsInfo = {"maxSensor": max(sensors, key=sensors.get), "maxValue":sensors.get(MAX), "minSensor":min(sensors, key=sensors.get), "minValue":sensors.get(MIN)  }
            robotInfo[0] = leftMotor.getVelocity()
            robotInfo[1] = rightMotor.getVelocity()

            
            ############################# STATES ############################
            ############################# teastState #########################
            if STATE == "teastState": # for now the angel is 0.
                print(STATE)
            ############################# Forward ###########################
            if STATE == "Forward":
                if LS0 > 750  or LS1 > 750:
                    spinInfo["angel"] = 12.5
                    targetEreaFound = True
                    spinInfo["pointPeriod"] = currentMotorInfo["rightMotor"]
                    spinInfo["direction"] = "left"
                    spinInfo["duration"] = 12.5 + cycle*3.45
                    targetPosition["x"] = 0.0
                    targetPosition["y"] = 0.0
                    STATE = "Spin"
                    nextState = "Search"
                    timerPeriod = robot.getTime()
                    spinInfo["spinMethod"] = "stepping"
                else:
                    leftMotor.setVelocity(motorSpeed)
                    rightMotor.setVelocity(motorSpeed)
                    if  not targetEreaFound:
                        if pre > 0.2:
                            STATE = "Back"
                            nextState = "Forward"
                            if turnOtherSide % 4 == 0 :# Turn left 90 dgree every 4 times
                                spinInfo["direction"] = "left"
                                spinInfo["pointPeriod"] = currentMotorInfo["rightMotor"]
                            else:
                                spinInfo["direction"] = "right"
                                spinInfo["pointPeriod"] = currentMotorInfo["leftMotor"]
                            spinInfo["spinMethod"] = "stepping"
                            spinInfo["duration"] = 6.2 + cycle*3.45
                            turnOtherSide += 1
                            timerPeriod = robot.getTime()
                if gs0 > 770 or gs1 > 770:
                    if gs0 > gs1:
                        spinInfo["direction"] = "right"
                        spinInfo["pointPeriod"] = currentMotorInfo["leftMotor"]
                    else:
                        spinInfo["direction"] = "left"
                        spinInfo["pointPeriod"] = currentMotorInfo["rightMotor"]
                    spinInfo["duration"] = 6.2 + cycle*3.45 # => 90 degree
                    spinInfo["spinMethod"] = "stepping"
                    timerPeriod = robot.getTime()
                    STATE = "Back"
                    nextState = "Forward"
            ############################# Search ###########################
            elif STATE == "Search":
                if locationInfo["round"] ==0:# start measuring...
                    leftMotor.setVelocity(motorSpeed)
                    rightMotor.setVelocity(motorSpeed)
                    if movingInfo["rightTurnCount"] %2 ==0:
                        if movingInfo["rightTurnCount"]/2 % 2 ==0:
                            targetPosition["y"] += cycle
                            Axse = "incY"
                        else:
                            targetPosition["y"] -= cycle
                            locationInfo["hight"] += cycle
                            Axse = "decY"
                    if movingInfo["leftTurnCount"] %2 !=  0:
                        if (movingInfo["leftTurnCount"]+1)/2 % 2 !=  0:
                            targetPosition["x"] += cycle
                            locationInfo["width"] += cycle
                            Axse = "incX"
                        else:
                            targetPosition["x"] -= cycle
                            Axse = "decX"
                    if (gs0 > 770 or gs1 > 770) or (targetPosition["y"] <0 or targetPosition["x"] < 0):
                        spinInfo["duration"] = 6.4 + cycle*3.45
                        spinInfo["direction"] = "right"
                        movingInfo["rightTurnCount"] +=1
                        movingInfo["leftTurnCount"] +=1
                        if movingInfo["rightTurnCount"] % 4 ==0:
                            locationInfo["round"] +=1
                            movingInfo["direction"] = "forwardY"
                            if targetPosition["y"]<0:
                                movingInfo["destanceY"] = locationInfo["hight"] - movingInfo["decreasment"] + targetPosition["y"]
                            else:
                                movingInfo["destanceY"] = locationInfo["hight"] - movingInfo["decreasment"] - targetPosition["y"]
                            if targetPosition["x"]<0:
                                movingInfo["destanceX"] = locationInfo["width"] - movingInfo["decreasment"] + targetPosition["x"]
                            else:
                                movingInfo["destanceX"] = locationInfo["width"] - movingInfo["decreasment"] - targetPosition["x"]
                        spinInfo["pointPeriod"] = currentMotorInfo["leftMotor"]
                        STATE = "Back"
                        timerPeriod = robot.getTime() + 1
                        nextState = "Search"
                else:
                    leftMotor.setVelocity(motorSpeed)
                    rightMotor.setVelocity(motorSpeed)
                    if movingInfo["direction"] == "forwardY":
                        targetPosition["y"] +=cycle
                        Axse = "incY"
                        if targetPosition["y"] > movingInfo["destanceY"]:
                            movingInfo["direction"] = "forwardX"
                            movingInfo["rightTurnCount"] +=1
                            STATE = "Back"
                            nextState = "Search"
                    elif movingInfo["direction"] == "forwardX":
                        targetPosition["x"] +=cycle
                        Axse = "incX"
                        if targetPosition["x"] > movingInfo["destanceX"]:
                            movingInfo["direction"] = "backY"
                            movingInfo["rightTurnCount"] +=1
                            STATE = "Back"
                            nextState = "Search"
                    elif movingInfo["direction"] == "backY":
                        targetPosition["y"] -=cycle
                        Axse = "decY"
                        if targetPosition["y"] < 0:
                            movingInfo["direction"] = "backX"
                            movingInfo["rightTurnCount"] +=1
                            STATE = "Back"
                            nextState = "Search"
                    elif movingInfo["direction"] == "backX":# Last round
                        
                        targetPosition["x"] -= cycle
                        Axse = "decX"
                        if targetPosition["x"] <0 :
                            movingInfo["direction"] = "forwardY"
                            locationInfo["round"] +=1
                            Axse = "incY"
                            movingInfo["rightTurnCount"] +=1
                            if targetPosition["y"]<0:
                                movingInfo["destanceY"] = locationInfo["hight"] - movingInfo["decreasment"] + targetPosition["y"]
                            else:
                                movingInfo["destanceY"] = locationInfo["hight"] - movingInfo["decreasment"] - targetPosition["y"]
                            if targetPosition["x"]<0:
                                movingInfo["destanceX"] = locationInfo["width"] - movingInfo["decreasment"] + targetPosition["x"]
                            else:
                                movingInfo["destanceX"] = locationInfo["width"] - movingInfo["decreasment"] - targetPosition["x"]
                            # movingInfo["destanceY"] = locationInfo["hight"] - movingInfo["decreasment"] - targetPosition["y"]
                            # movingInfo["destanceX"] = locationInfo["width"] - movingInfo["decreasment"] - targetPosition["x"]
                            STATE = "Back"
                            nextState = "Search"
                    timerPeriod = robot.getTime()
                    spinInfo["duration"] = 1.25
                    spinInfo["direction"] = "right"
                    nextState = "Search"
                    spinInfo["spinMethod"] = "timeDuration"
                if pre > 0.2 and Axse !="decX":
                    STATE = "Avoid"
                    # STATE = "Spin"
                    nextState = "Search"
                    spinInfo["pointPeriod"] = currentMotorInfo["rightMotor"]
                    timerPeriod = robot.getTime()
                if targetPosition["x"]<0 and targetPosition["y"]<0:
                    print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
                    spinInfo["duration"] = 6.4 + cycle*3.45
                    spinInfo["direction"] = "right"
                    nextState = "Search"
                if locationInfo["round"] >4:
                    STATE = "Stop"
            ############################# goToTarget #####################
            elif STATE == "goToTarget":
                leftMotor.setVelocity(motorSpeed)
                rightMotor.setVelocity(motorSpeed)
                if movingOnY:
                    targetPosition["y"] -= cycle
                else:
                    targetPosition["x"] -= cycle  
                if targetPosition["y"] <0:
                    targetPosition["y"] = 0
                    STATE = "Stop" 
            ############################# Stop ########################### 
            elif STATE == "Stop":
                leftMotor.setVelocity(0)
                rightMotor.setVelocity(0)
                targetEreaFound = True
                timerPeriod = robot.getTime()
            ############################# Spin ###########################
            elif STATE == "Spin":#12600ms for 180 Degree if Motor Speed =1.
                if spinInfo["spinMethod"] == "timeDuration":
                    if spinInfo["direction"] == "left":
                        leftMotor.setVelocity(-5)
                        rightMotor.setVelocity(5)
                    else:
                        leftMotor.setVelocity(5)
                        rightMotor.setVelocity(-5)
                    if robot.getTime() - timerPeriod >  spinInfo["duration"] :
                        STATE = nextState
                        timerPeriod = robot.getTime()
                else:# Spin Method here is according to left or right motor step
                    if spinInfo["direction"] == "left":
                        leftMotor.setVelocity(-motorSpeed)
                        rightMotor.setVelocity(motorSpeed)
                        if currentMotorInfo["rightMotor"] - spinInfo["pointPeriod"]>spinInfo["duration"]:
                            STATE = nextState
                            spinInfo["pointPeriod"] = 0.0
                    else:
                        if targetEreaFound:
                            leftMotor.setVelocity(4)
                            rightMotor.setVelocity(-1) # should Integer
                        else:
                            leftMotor.setVelocity(motorSpeed)
                            rightMotor.setVelocity(-motorSpeed)
                        if currentMotorInfo["leftMotor"] - spinInfo["pointPeriod"] > spinInfo["duration"]:
                            STATE = nextState
                            spinInfo["pointPeriod"] = 0.0
            ############################# Back ###########################
            elif STATE == "Back":
                leftMotor.setVelocity(-2)
                rightMotor.setVelocity(-2)
                print("#####################################")
                print(locationInfo)
                if  locationInfo["round"] == 0 and (movingInfo["rightTurnCount"] > 1 ):
                        if Axse == "incY":
                            targetPosition["y"] += -cycle
                            locationInfo["hight"] += -cycle
                        elif Axse == "decY":
                            targetPosition["y"] -= -cycle
                            locationInfo["hight"] += -cycle
                        elif Axse == "incX":
                            targetPosition["x"] += -cycle
                            locationInfo["width"] += -cycle
                        elif Axse == "decX":
                            targetPosition["x"] -= -cycle
                else:
                    if Axse == "incY":
                        targetPosition["y"] += -cycle
                    elif Axse == "decY":
                        targetPosition["y"] -= -cycle
                    elif Axse == "incX":
                        targetPosition["x"] += -cycle
                    elif Axse == "decX":
                        targetPosition["x"] -= -cycle
                if robot.getTime() - timerPeriod > 1:
                    timerPeriod = robot.getTime()
                    STATE = "Spin"
            ############################# Check ###########################
            elif STATE == "Check":
                leftMotor.setVelocity(1)
                rightMotor.setVelocity(-1)
            ############################# Avoid STATE ###########################
            elif STATE == "Avoid":
                if robot.getTime() - timerPeriod < 0.5:
                    time = time + robot.getTime()
                    leftMotor.setVelocity(-5) #  VVVVVV
                    rightMotor.setVelocity(-5)
                    if  locationInfo["round"] == 0 and (movingInfo["rightTurnCount"] > 1 ):
                        if Axse == "incY":
                            targetPosition["y"] += -cycle
                            locationInfo["hight"] += -cycle
                        elif Axse == "decY":
                            targetPosition["y"] -= -cycle
                            locationInfo["hight"] += -cycle
                        elif Axse == "incX":
                            targetPosition["x"] += -cycle
                            locationInfo["width"] += -cycle
                        elif Axse == "decX":
                            targetPosition["x"] -= -cycle
                    else:
                        if Axse == "incY":
                            targetPosition["y"] += -cycle
                        elif Axse == "decY":
                            targetPosition["y"] -= -cycle
                        elif Axse == "incX":
                            targetPosition["x"] += -cycle
                        elif Axse == "decX":
                            targetPosition["x"] -= -cycle
                    print("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP")
                elif robot.getTime() - timerPeriod > 0.5 and robot.getTime() - timerPeriod <1.75:
                    leftMotor.setVelocity(-5) # <=========
                    rightMotor.setVelocity(5)
                elif robot.getTime() - timerPeriod >1.75 and robot.getTime() - timerPeriod < 3:
                    leftMotor.setVelocity(5) # ^^^^^
                    rightMotor.setVelocity(5)
                    # if movingInfo["direction"] == "forwardY" or movingInfo["direction"] == "backY" :
                    #     movingInfo["currentY"] +=cycle
                    # if movingInfo["direction"] == "forwardX" or movingInfo["direction"] == "backX" :
                    #      movingInfo["currentX"] +=cycle
                elif robot.getTime() - timerPeriod > 3 and robot.getTime() - timerPeriod <4.25:
                    leftMotor.setVelocity(5) # =============>
                    rightMotor.setVelocity(-5)
                elif robot.getTime() - timerPeriod >4.25 and robot.getTime() - timerPeriod < 5.5:
                    leftMotor.setVelocity(5) # ^^^^^
                    rightMotor.setVelocity(5)
                    # Increase or decrease Y
                    # if nextState == "Search":
                    if  locationInfo["round"] == 0 and (movingInfo["rightTurnCount"] == 1 or movingInfo["rightTurnCount"] == 2):
                        if Axse == "incY":
                            targetPosition["y"] += cycle
                            locationInfo["hight"] += cycle
                        elif Axse == "decY":
                            targetPosition["y"] -= cycle
                            locationInfo["hight"] += cycle
                        elif Axse == "incX":
                            targetPosition["x"] += cycle
                            locationInfo["width"] += cycle
                        elif Axse == "decX":
                            targetPosition["x"] -= cycle
                    else:
                        if Axse == "incY":
                            targetPosition["y"] += cycle
                        elif Axse == "decY":
                            targetPosition["y"] -= cycle
                        elif Axse == "incX":
                            targetPosition["x"] += cycle
                        elif Axse == "decX":
                            targetPosition["x"] -= cycle
                elif robot.getTime() - timerPeriod > 5.5 and robot.getTime() - timerPeriod < 6.75:
                    Axse = ""
                    leftMotor.setVelocity(5) # ==========>
                    rightMotor.setVelocity(-5)
                elif robot.getTime() - timerPeriod > 6.75 and robot.getTime() - timerPeriod < 8:
                    leftMotor.setVelocity(5) # ^^^^
                    rightMotor.setVelocity(5)
                elif robot.getTime() - timerPeriod > 8 and robot.getTime() - timerPeriod < 9.25:
                    leftMotor.setVelocity(-5)
                    rightMotor.setVelocity(5)
                if robot.getTime() - timerPeriod > 9.25:
                    timerPeriod = robot.getTime()
                    STATE = nextState
                if targetPosition["x"]<0 and targetPosition["y"]<0:
                    print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
                    spinInfo["duration"] = 6.4 + cycle*3.45
                    spinInfo["direction"] = "right"
                    nextState = "Search"
            ############################# maxDistance ###########################
            elif STATE == "maxDistance":
                leftMotor.setVelocity(0)
                rightMotor.setVelocity(0)
                if sensorsInfo["maxValue"] > 0:
                    STATE = "Search"
                    timerPeriod = robot.getTime()
                print("It seems that here is the Maximal Distance!!!")
            if hs0 > 0 or hs1 > 0 or hs2 > 0 or hs3 > 0 or hs4 > 0 or hs5 > 0 or hs6 > 0:
                robotInfo[3] = 1
            else:
                robotInfo[3] = 0
            if gs0 > 770 or gs1 > 770:
                robotInfo[4] = 1
            else:
                robotInfo[4] = 0
            if pre > 0.5:
                robotInfo[5] = 1
            else:
                robotInfo[5] = 0
            if LS0 > 750 or LS1 > 750:
                robotInfo[6] = 1
            else:
                robotInfo[6] = 0
            print(robotInfo)

            step = ( leftMotor.getPositionSensor().getValue() - step)
            if robot.step(TIME_STEP) == -1:  # quit the loop when Webots is about to quit
                break

run()